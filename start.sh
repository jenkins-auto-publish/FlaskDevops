#!/bin/sh

#进入工作区目录
cd /workspace/flask_devops_workspace
#构建镜像
docker build -t flask_devops  .
#停止容器
docker stop flask_devops
#启动容器
docker run --rm -d --name flask_devops -p8081:5000 flask_devops