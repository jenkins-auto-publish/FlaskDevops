#基于python3.5.2构建
from registry.cn-hangzhou.aliyuncs.com/moxinqian/python352

#端口
ARG PORT=5000

#设置时区
ARG TIME_ZONE=Asia/Shanghai

COPY flask_devops /app/flask_devops

RUN pip install -i https://pypi.douban.com/simple --upgrade pip


RUN pip install -i https://pypi.douban.com/simple -r /app/flask_devops/requirements.txt

#健康检查
HEALTHCHECK --interval=5s --timeout=3s  CMD curl -fs http://localhost:5000 || exit 1

EXPOSE ${PORT}

#开机自动启动
ENTRYPOINT uwsgi --ini /app/flask_devops/start.ini